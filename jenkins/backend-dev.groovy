pipeline {
    agent {
        label 'linux'
    }
    environment {
        ENV = "dev"
        BRANCH = "dev"
        PROJECT = "backend"
        PROJECT_KEY= "hapster-backend"
        REPO_DIR = "api.hapster"
        DEPLOYMENT_SERVER_IP = "10.114.0.7"
        DEPLOYMENT_SERVER_PORT = "53974"
        SERVICE_NAME = "laravel"
        REPO_URL = "https://gitlab.hapster.dev/hapster-4.0/api.hapster.git"
        REGISTRY_URL = "gitlab.hapster.dev:5050"
        REGISTRY_CREDENTIALS = credentials('hapster-4.0-credentials')
        IMAGE_NAME = "${REGISTRY_URL}/hapster-4.0/api.hapster/${SERVICE_NAME}:${ENV}"
    }
    stages {
        stage('Code Checkout') {
            steps {
                dir("${REPO_DIR}") {
                checkout scmGit(branches: [[name: "*/${BRANCH}"]], 
                userRemoteConfigs: [[credentialsId: 'hapster-4.0-credentials', url: "${REPO_URL}"]])
                }
            }
        }
        stage('Code Quality Check') {
            steps {
                catchError (buildResult: 'UNSTABLE', stageResult: 'UNSTABLE') {
                    sh '''
                    cd $REPO_DIR
                    sonar-scanner \
                        -Dsonar.projectKey=$PROJECT_KEY \
                        -Dsonar.sources=.
                    ''' 
                }
            }
        }
        stage('Build') {
            steps {
                sh 'docker build -t $IMAGE_NAME .'
            }
        }
        stage('Push') {
            steps {
                sh 'echo $REGISTRY_CREDENTIALS_PSW | docker login $REGISTRY_URL -u $REGISTRY_CREDENTIALS_USR --password-stdin'
                sh 'docker push $IMAGE_NAME'
            }
        }
        stage('Deploy') {
            steps {
                sshagent(['op-build-key']) {
                    sh '''
                    ssh -o StrictHostKeyChecking=no -p $DEPLOYMENT_SERVER_PORT -l op $DEPLOYMENT_SERVER_IP "
                    kubectl rollout restart -n ${ENV} deployments ${SERVICE_NAME}-depl"
                    '''
                }
            }
        }
    }
    post {
        always {
            notifySlack(currentBuild.currentResult)
            sh '''
            docker system prune -f
            '''
        }
    }
}

def getChangeCommentsAndAuthors() {
    def changeInfo = ''
    currentBuild.changeSets.each { changeSet ->
        changeSet.items.each { change ->
            def commitId = change.commitId
            def changeComment = change.comment.trim()
            def author = change.author.fullName
            changeInfo += "• Commit ID: ${commitId}\n    Author: ${author}\n    Changes: ${changeComment}\n\n"
        }
    }
    
    return changeInfo ?: "• No changes in this deployment :empty_nest:"
}

def notifySlack(String buildStatus)
{
    def color
    def message
    if (buildStatus == 'SUCCESS') {
        color = 'good'
        message = """
        • The job <${env.BUILD_URL}|${env.BUILD_NUMBER}> of *${env.JOB_NAME}* successfully deployed to development :rocket:
${getChangeCommentsAndAuthors()}
        """
    }
    else if (buildStatus == 'UNSTABLE') {
        color = 'warning'
        message = """
        • The job <${env.BUILD_URL}|${env.BUILD_NUMBER}> of *${env.JOB_NAME}* successfully deployed to development, but check for errors :neutral_face:
${getChangeCommentsAndAuthors()}
        """
    }
    else if (buildStatus == 'FAILURE') {
        color = 'danger'
        message = """
        The job <${env.BUILD_URL}|${env.BUILD_NUMBER}> of *${env.JOB_NAME}* failed to deploy to development :scream: 
        """
    }
    slackSend(color: color, message: message)
}