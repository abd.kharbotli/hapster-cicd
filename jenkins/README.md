# Hapster CICD

## Overview

This repository contains Jenkinsfiles written in Groovy. Jenkinsfiles are used to define pipeline jobs in Jenkins, allowing you to automate your software development and deployment processes.

## Repository Structure

- **`backend-dev.groovy`**: Backend dev branch.
- **`backend-stage.groovy`**: backend staging branch.
- **`frontend-dev.groovy`**: frontend dev branch.
- **`backend-stage.groovy`**: frontend staging branch.
- **`mobile-dev.groovy`**: mobile dev branch.

## Usage

1. Clone this repository to your local machine:

    ```shell
    git clone https://github.com/your-username/groovy-jenkins-examples.git
    ```

2. Customize the Jenkinsfile according to your specific project needs. You can add or modify stages, define build and deployment steps, and configure other pipeline elements.

3. Configure your Jenkins instance to use the selected Jenkinsfile for your project's pipeline.

## Contributing

If you'd like to contribute to this repository by adding more Jenkinsfile or improving existing ones, please follow these steps:

1. Clone the repository.

2. Create a new branch for your changes:

    ```shell
    git pull
    git checkout -b localbranch main
    ```

3. Make your changes to the Jenkinsfiles or add new ones.

4. Commit your changes and push them to your fork:

    ```shell
    git commit -m "Add new Jenkinsfile"
    git push -u origin localbranch
    ```

5. Create a merge request to propose your changes.

