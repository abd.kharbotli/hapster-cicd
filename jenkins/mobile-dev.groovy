pipeline {
    agent none
    environment {
        ENV = "dev"
        BRANCH = "dev"
        PROJECT = "mobile"
        PROJECT_KEY= "hapster-mobile"
        REPO_DIR = "hapster-mobile"
        REPO_URL = "https://gitlab.hapster.dev/hapster-4.0/hapster-mobile.git"
        APK_NAME = "hapster-${env.BUILD_NUMBER}.apk"
        MSIX_NAME = "hapster-${env.BUILD_NUMBER}.msix"
    }
    stages {
        stage ('Code Checkout') {
            parallel {
                stage('Linux') {
                    agent {
                        label ('linux')
                    }
                    steps {
                        dir("${REPO_DIR}") {
                        checkout scmGit(branches: [[name: "*/${BRANCH}"]], 
                        userRemoteConfigs: [[credentialsId: 'hapster-4.0-credentials', url: "${REPO_URL}"]])
                        }
                    }
                }
                stage('Windows') {
                    agent {
                        label ('windows')
                    }
                    steps {
                        dir("${REPO_DIR}") {
                        checkout scmGit(branches: [[name: "*/${BRANCH}"]], 
                        userRemoteConfigs: [[credentialsId: 'hapster-4.0-credentials', url: "${REPO_URL}"]])
                        }
                    }
                }
            }
        }
        stage('Code Quality Check') {
            agent {
                label 'linux '
            }
            steps {
                catchError (buildResult: 'UNSTABLE', stageResult: 'UNSTABLE') {
                    sh '''
                    cd $REPO_DIR
                    sonar-scanner \
                        -Dsonar.projectKey=$PROJECT_KEY \
                        -Dsonar.sources=.
                    ''' 
                }
            }
        }
        stage('Build') {
            parallel {
                stage('Android') {
                    agent {
                        label 'linux '
                    }
                    steps {
                       sh '''
                        cd $REPO_DIR/android
                        fastlane pub_get
                        fastlane build
                        ''' 
                        sh '''
                        mv $REPO_DIR/build/app/outputs/apk/release/app-release.apk releases/$APK_NAME
                        '''
                    }
                }
                stage('Windows') {
                    agent {
                        label 'windows '
                    }
                    steps {
                       bat '''
                        cd %REPO_DIR%
                        flutter pub get
                        ''' 
                        bat '''
                        cd %REPO_DIR%
                        flutter pub add --dev msix
                        '''
                        bat '''
                        cd %REPO_DIR%
                        flutter pub run msix:create --install-certificate false
                        ''' 
                        bat '''
                        move %REPO_DIR%\\build\\windows\\runner\\Release\\hapster.msix releases\\%MSIX_NAME%
                        '''
                    }
                }
            }
        }
        stage('Artifacts') {
            parallel {
                stage('Android') {
                    agent {
                        label 'linux'
                    }
                    steps {
                       archiveArtifacts artifacts: "releases/${APK_NAME}", followSymlinks: false, onlyIfSuccessful: true
                    }
                }
                stage('Windows') {
                    agent {
                        label 'windows'
                    }
                    steps {
                       archiveArtifacts artifacts: "releases/${MSIX_NAME}", followSymlinks: false, onlyIfSuccessful: true
                    }
                }
            }
        }
        stage('Cleaning Build') {
            parallel {
                stage('Android') {
                    agent {
                        label 'linux '
                    }
                    steps {
                        sh '''
                        cd $REPO_DIR/android
                        fastlane clean
                        '''
                    }
                }
                stage('Windows') {
                    agent {
                        label 'windows'
                    }
                    steps {
                        bat  '''
                        cd %REPO_DIR%
                        flutter clean
                        '''
                    }
                }
            }
        }
    }
    post {
        always {
            notifySlack(currentBuild.currentResult)
            node ('linux') {
                sh '''
                cd releases
                ls -t | tail -n +3 | xargs rm --
                '''
            }
            node ('windows') {
                bat  '''
                cd releases
                for /f "skip=2 eol=: delims=" %%F in ('dir /b /o-d /a-d *') do @del "%%F"
                '''
            }
        }
    }
}

def getChangeCommentsAndAuthors() {
    def changeInfo = ''
    currentBuild.changeSets.each { changeSet ->
        changeSet.items.each { change ->
            def commitId = change.commitId
            def changeComment = change.comment.trim()
            def author = change.author.fullName
            changeInfo += "• Commit ID: ${commitId}\n    Author: ${author}\n    Changes: ${changeComment}\n\n"
        }
    }
    
    return changeInfo ?: "• No changes in this build :empty_nest:"
}

def notifySlack(String buildStatus)
{
    def color
    def message
    if (buildStatus == 'SUCCESS') {
        color = 'good'
        message = """
        • The job <${env.BUILD_URL}|${env.BUILD_NUMBER}> of *${env.JOB_NAME}* completed building successfully :rocket:
• You can download Android APK from here <https://jenkins.staging-hsp.com/job/mobile-dev/${env.BUILD_NUMBER}/artifact/releases/${APK_NAME}|${APK_NAME}>
• You can download Windows MSIX from here <https://jenkins.staging-hsp.com/job/mobile-dev/${env.BUILD_NUMBER}/artifact/releases/${MSIX_NAME}|${MSIX_NAME}>
${getChangeCommentsAndAuthors()}
        """
    }
    else if (buildStatus == 'UNSTABLE') {
        color = 'warning'
        message = """
        • The job <${env.BUILD_URL}|${env.BUILD_NUMBER}> of *${env.JOB_NAME}* successfully deployed to development, but check for errors :neutral_face:
${getChangeCommentsAndAuthors()}
        """
    }
    else if (buildStatus == 'FAILURE') {
        color = 'danger'
        message = """
        The job <${env.BUILD_URL}|${env.BUILD_NUMBER}> of *${env.JOB_NAME}* failed to build :scream: 
        """
    }
    slackSend(color: color, message: message)
}