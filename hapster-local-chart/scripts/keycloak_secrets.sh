#!/bin/bash

#set -x

# Function to prompt for username and password
get_credentials() {
    read -rep "Enter username: " username
    read -rsep "Enter password: " password
}

# Function to retrieve the access token
get_access_token() {
    response=$(curl -s -X POST "${hostname}/realms/master/protocol/openid-connect/token" \
        -H 'Accept: application/json' \
        -H 'Content-Type: application/x-www-form-urlencoded' \
        -H 'cache-control: no-cache' \
        -d "grant_type=password&username=${username}&password=${password}&client_id=admin-cli")

    # Check for an error response
    if echo "$response" | jq -e '.error' &> /dev/null; then
        error_message=$(echo "$response" | jq -r '.error_description')
        echo "Error: $error_message"
        exit 1
    else
        access_token=$(echo "$response" | jq -r .access_token)
    fi
}

# Function to create a new client
create_client() {
    access_token="$1"
    client_id="$2"

    create_response=$(curl -s -X POST "${hostname}/admin/realms/master/clients" \
        -H 'accept: application/json' \
        -H "authorization: Bearer ${access_token}" \
        -H 'content-type: application/json' \
        -d '{"protocol":"openid-connect","clientId":"'${client_id}'","publicClient":false,"authorizationServicesEnabled":false,"serviceAccountsEnabled":false,"implicitFlowEnabled":false,"directAccessGrantsEnabled":true,"standardFlowEnabled":true,"frontchannelLogout":true,"attributes":{"oauth2.device.authorization.grant.enabled":true,"oidc.ciba.grant.enabled":false},"alwaysDisplayInConsole":false}')

    # Check for an error response
    if [[ -n "$create_response" ]]; then
        if echo "$create_response" | jq -e '.errorMessage' &> /dev/null; then
            error_message=$(echo "$create_response" | jq -r '.errorMessage')
            echo "Error: $error_message"
            exit 1
        fi
    fi
}

# Function to retrieve the client secret
get_client_secret() {
    access_token="$1"
    client_id="$2"

    # Retrieve client information to get the client secret
    client_info_response=$(curl -s -X GET "${hostname}/admin/realms/master/clients?clientId=${client_id}" \
        -H 'accept: application/json' \
        -H "authorization: Bearer ${access_token}")

    # Check for an error response
    if echo "$client_info_response" | jq -e '.error' &> /dev/null; then
        error_message=$(echo "$client_info_response" | jq -r '.error')
        echo "Error: $error_message"
        exit 1
    else
        client_secret=$(echo "$client_info_response" | jq -r '.[0].secret')
    fi
}

# Function to retrieve the master public key
get_public_key() {
    access_token="$1"

    # Retrieve client information to get the client secret
    realm_info_response=$(curl -s -X GET "${hostname}/realms/master" \
        -H 'accept: application/json')

    public_key=$(echo "$realm_info_response" | jq -r '.public_key')
}


update_yaml_file() {
    yaml_file="$4"

    # Update the YAML file using yq
    client_id="$1" yq --inplace '.secrets.keycloakClientID = strenv(client_id)' "$yaml_file"
    encoded_client_secret="$2" yq --inplace '.secrets.keycloakClientSecret = strenv(encoded_client_secret)' "$yaml_file"
    encoded_public_key="$3" yq --inplace '.secrets.keycloakRealmPublicKey = strenv(encoded_public_key)' "$yaml_file"
}

main() {
    read -rep "Enter hostname (e.g., https://kc.example.com): " hostname

    get_credentials

    get_access_token "$username" "$password"

    read -rep "Enter client ID: " client_id
    create_client "$access_token" "$client_id"

    get_client_secret "$access_token" "$client_id"

    get_public_key

    # Encode the client ID in base64
    encoded_client_id=$(echo -n "$client_id" | base64)

    # Encode the client secret in base64
    encoded_client_secret=$(echo -n "$client_secret" | base64)

    # Encode the public key in base64
    encoded_public_key=$(echo -n "$public_key" | base64 -w 0)

    # Ask the user for the file path
    read -rep "Enter the path to the YAML file: " yaml_path

    # Update the YAML file with the client_id and encoded_client_secret
    update_yaml_file "$encoded_client_id" "$encoded_client_secret" "$encoded_public_key" "$yaml_path"
}

# Call the main function
main
