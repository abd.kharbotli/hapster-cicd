## Introduction

This charts installs hapster app and its components into a [Kubernetes](http://kubernetes.io) cluster using the [Helm](https://helm.sh).

## Prerequisites

- Kubernetes 1.28.2
- helm 3.13.0
- Existing directories for PVs

## Configuration

The following tables lists the configurable parameters of this chart and their default values.

| Parameter                             | Description                               | Default                                  |
| ------------------------------------- | ----------------------------------------- | ---------------------------------------- |
| `laravel.env`                         | Laravel environment(eg. local,production) | `local`                                  |
| `laravel.appURL`                      | The external URL of the API               | `https://api.hapster.com`                |
| `nextjs.hostURL`                      | API URL                                   | `https://api.hapster.com`                |
| `keycloak.hostName`                   | Keycloak external URL for administration  | `kc.hapster.com`                         |
| `laravel.port`                        | Node port for the laravel service         | `30011`                                  |
| `keycloak.port`                       | Node port for the keycloak service        | `30012`                                  |
| `nextjs.port`                         | Node port for the admin panel service     | `30013`                                  |
| `laravel.raplicas`                    | Number of API replicas                    | `1`                                      |
| `keycloak.raplicas`                   | Number of keycloak replicas               | `1`                                      |
| `nextjs.raplicas`                     | Number of the admin panel replicas        | `1`                                      |
| `laravel.nodeName`                    | Node name for Laravel PV                  | `k8s`                                    |
| `keycloakDatabase.nodeName`           | Node name for Keycloak DB PV              | `k8s`                                    |
| `laravelDatabase.nodeName`            | Node name for Laravel DB PV               | `k8s`                                    |
| `laravel.capacity`                    | Capacity for Laravel PV                   | `10Gi`                                   |
| `keycloakDatabase.capacity`           | Capacity for Keycloak DB PV               | `10Gi`                                   |
| `laravelDatabase.capacity`            | Capacity for Laravel DB PV                | `10Gi`                                   |
| `laravel.path`                        | Path for Laravel PV                       | `/mnt/dev-laravel-pv/`                   |
| `laravelDatabase.path`                | Path for Laravel database PV              | `/mnt/dev-laravel-db-pv/`                |
| `keycloakDatabase.path`               | Path for Laravel database PV              | `/mnt/dev-keycloak-db-pv/`               |
| `laravelDatabase.db`                  | Database name for Laravel                 | `laravel`                                |
| `keycloakDatabase.db`                 | Database name for Keycloak                | `keycloak`                               |
| `laravel.hadoopURL`                   | Hadoop HDFS URL                           | `hadoop.hapster.com`                     |
| `laravel.hadoopPort`                  | Hadoop HDFS port                          | `443`                                    |
| `laravel.HadoopRootDir`               | Hadoop HDFS root directory                | `root/dev/`                              |
| `mailConfiguration.protocol`          | Protocol for mail server                  | `smtp`                                   |
| `mailConfiguration.host`              | Mail server host                          | `smtp.hapster.com`                       |
| `mailConfiguration.port`              | Mail server port	                        | `587`                                    |
| `mailConfiguration.encryption`        | Encryption type for mail server           | `TLS`                                    |
| `mailConfiguration.sender`            | Sender email address                      | `admin@hapster.com`                      |
| `mailConfiguration.prettyName`        | Sender name	                            | `Hapster`                                |
| `KC_HOSTNAME`                         | Keycloak external URL for administration  | `kc.hapster.com`                         |
| `images.laravelDeployment`            | Laravel container image                   | `registry.hapster.dev/laravel:1.0.0`     |
| `images.keycloakDeployment`           | Keycloak container image                  | `registry.hapster.dev/keycloak:latest`   |
| `images.postgresDB`                   | PostgreSQL container image                | `registry.hapster.dev/postgres:latest`   |
| `images.nextjsDeployment`             | Next.js container image                   | `registry.hapster.dev/nextjs:1.0.0`      |


## Secrets Configuration

The following parameters configure secrets for your application:

| Parameter                                 | Description                               |
| ----------------------------------------- | ----------------------------------------- |
| `secrets.appKey`                          | Laravel key                               |
| `secrets.keycloakClientID`                | Keycloak client ID for API                |
| `secrets.keycloakClientSecret`            | Keycloak client secret for API            |
| `secrets.keycloakAdminUsername`           | Keycloak admin username                   |
| `secrets.keycloakAdminPassword`           | Keycloak admin password                   |
| `secrets.keycloakRealmPublicKey`          | Keycloak realm public key for API         |
| `secrets.keycloakDBUsername`              | Keycloak database username                |
| `secrets.keycloakDBPassword`              | Keycloak database password                |
| `secrets.laravelDBUsername`               | Laravel database username                 |
| `secrets.laravelDBPassword`               | Laravel database password                 |
| `secrets.mailSenderUsername`              | Mail sender username for API              |
| `secrets.mailSenderPassword`              | Mail sender password for API              |
| `secrets.lrsUser`                         | Learning Record Store user                |
| `secrets.lrsPassword`                     | Learning Record Store password            |
| `registryCredentials`                     | Credentials for private registry          |

## Installing the Chart

To install the chart with the release name `my-release` you have got to do it like this:

```console
$ helm install my-release /path/to/chart/ \
    --set secrets.appKey=<key> \
    --set secrets.keycloakAdminUsername=<keycloak_admin_user> \
    --set secrets.keycloakAdminPassword=<keycloak_admin_password> \
    --set secrets.keycloakDBUsername=<keycloak_db_username> \
    --set secrets.keycloakDBPassword=<keycloak_db_password> \
    --set secrets.laravelDBUsername=<laravel_db_username> \
    --set secrets.laravelDBPassword=<laravel_db_password> \
    --set secrets.mailSenderUsername=<email_username> \
    --set secrets.mailSenderPassword=<email_password> \
    --set secrets.lrsUser=<lrs_user> \
    --set secrets.lrsPassword=<lrs_password> \
    --set registryCredentials=<registry_creds>
```

The command deploys the given chart in the default configuration. The [configuration](#configuration) section lists the parameters that can be configured during installation.

> **Tip**: List all releases using `helm list`

## Uninstalling the Chart

To uninstall/delete the `my-release` deployment:

```console
$ helm delete my-release
```

The command removes all the Kubernetes components associated with the chart and deletes the release.