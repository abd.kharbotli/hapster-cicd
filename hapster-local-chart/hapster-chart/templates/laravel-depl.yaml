apiVersion: apps/v1
kind: Deployment
metadata:
  name: laravel-depl
spec:
  replicas: {{ .Values.laravel.replicas }}
  selector:
    matchLabels:
      app: laravel
  template:
    metadata:
      labels:
        app: laravel
    spec:
      imagePullSecrets:
        - name: gitlab-registry-creds
      initContainers:
        - name: check-db-ready
          image: postgres:alpine
          command: ['/bin/sh', '-c', 'until pg_isready -h laravel-db-sts-0.laravel-db-svc -p 5432; do sleep 2; done;']
      initContainers:
        - name: check-keycloak-ready
          image: busybox:latest
          command: ['/bin/sh', '-c', 'until wget http://keycloak-svc-local:8080; do sleep 2; done;']
      containers:
        - name: laravel
          image: "{{ .Values.images.laravelDeployment }}"
          env:
            - name: APP_NAME
              valueFrom:
                configMapKeyRef:
                  name: hapster-cm
                  key: APP_NAME
            - name: APP_ENV
              valueFrom:
                configMapKeyRef:
                  name: hapster-cm
                  key: APP_ENV
            - name: APP_DEBUG
              valueFrom:
                configMapKeyRef:
                  name: hapster-cm
                  key: APP_DEBUG
            - name: APP_URL
              valueFrom:
                configMapKeyRef:
                  name: hapster-cm
                  key: APP_URL
            - name: KEYCLOAK_REALM
              valueFrom:
                configMapKeyRef:
                  name: hapster-cm
                  key: KEYCLOAK_REALM
            - name: KEYCLOAK_URL
              valueFrom:
                configMapKeyRef:
                  name: hapster-cm
                  key: KEYCLOAK_URL
            - name: KEYCLOAK_LOAD_USER_FROM_DATABASE
              valueFrom:
                configMapKeyRef:
                  name: hapster-cm
                  key: KEYCLOAK_LOAD_USER_FROM_DATABASE
            - name: KEYCLOAK_ALLOWED_RESOURCES
              valueFrom:
                configMapKeyRef:
                  name: hapster-cm
                  key: KEYCLOAK_ALLOWED_RESOURCES
            - name: KEYCLOAK_IGNORE_RESOURCES_VALIDATION
              valueFrom:
                configMapKeyRef:
                  name: hapster-cm
                  key: KEYCLOAK_IGNORE_RESOURCES_VALIDATION
            - name: KEYCLOAK_APPEND_DECODED_TOKEN
              valueFrom:
                configMapKeyRef:
                  name: hapster-cm
                  key: KEYCLOAK_APPEND_DECODED_TOKEN
            - name: LOG_CHANNEL
              valueFrom:
                configMapKeyRef:
                  name: hapster-cm
                  key: LOG_CHANNEL
            - name: LOG_DEPRECATIONS_CHANNEL
              valueFrom:
                configMapKeyRef:
                  name: hapster-cm
                  key: LOG_DEPRECATIONS_CHANNEL
            - name: LOG_LEVEL
              valueFrom:
                configMapKeyRef:
                  name: hapster-cm
                  key:   LOG_LEVEL
            - name: DB_CONNECTION
              valueFrom:
                configMapKeyRef:
                  name: hapster-cm
                  key: DB_CONNECTION
            - name: DB_HOST
              valueFrom:
                configMapKeyRef:
                  name: hapster-cm
                  key: DB_HOST
            - name: DB_PORT
              valueFrom:
                configMapKeyRef:
                  name: hapster-cm
                  key: DB_PORT
            - name: DB_DATABASE
              valueFrom:
                configMapKeyRef:
                  name: hapster-cm
                  key: DB_DATABASE
            - name: MAIL_MAILER
              valueFrom:
                configMapKeyRef:
                  name: hapster-cm
                  key: MAIL_MAILER
            - name: MAIL_HOST
              valueFrom:
                configMapKeyRef:
                  name: hapster-cm
                  key: MAIL_HOST
            - name: MAIL_PORT
              valueFrom:
                configMapKeyRef:
                  name: hapster-cm
                  key: MAIL_PORT
            - name: MAIL_ENCRYPTION
              valueFrom:
                configMapKeyRef:
                  name: hapster-cm
                  key: MAIL_ENCRYPTION
            - name: MAIL_FROM_ADDRESS
              valueFrom:
                configMapKeyRef:
                  name: hapster-cm
                  key: MAIL_FROM_ADDRESS
            - name: MAIL_FROM_NAME
              valueFrom:
                configMapKeyRef:
                  name: hapster-cm
                  key: MAIL_FROM_NAME
            - name: QUEUE_CONNECTION
              valueFrom:
                configMapKeyRef:
                  name: hapster-cm
                  key: QUEUE_CONNECTION
            - name: SESSION_DRIVER
              valueFrom:
                configMapKeyRef:
                  name: hapster-cm
                  key: SESSION_DRIVER
            - name: SESSION_LIFETIME
              valueFrom:
                configMapKeyRef:
                  name: hapster-cm
                  key: SESSION_LIFETIME
            - name: CACHE_DRIVER
              valueFrom:
                configMapKeyRef:
                  name: hapster-cm
                  key: CACHE_DRIVER      
            - name: REDIS_HOST
              valueFrom:
                configMapKeyRef:
                  name: hapster-cm
                  key: REDIS_HOST
            - name: REDIS_PORT
              valueFrom:
                configMapKeyRef:
                  name: hapster-cm
                  key: REDIS_PORT
            - name: LRS_URL
              valueFrom:
                configMapKeyRef:
                  name: hapster-cm
                  key: LRS_URL
            - name: APP_KEY
              valueFrom:
                secretKeyRef:
                  name: hapster-secret
                  key: APP_KEY
            - name: KEYCLOAK_ADMIN_USERNAME
              valueFrom:
                secretKeyRef:
                  name: hapster-secret
                  key: KEYCLOAK_ADMIN_USERNAME
            - name: KEYCLOAK_ADMIN_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: hapster-secret
                  key: KEYCLOAK_ADMIN_PASSWORD
            - name: KEYCLOAK_CLIENT_ID
              valueFrom:
                secretKeyRef:
                  name: hapster-secret
                  key: KEYCLOAK_CLIENT_ID
            - name: KEYCLOAK_CLIENT_SECRET
              valueFrom:
                secretKeyRef:
                  name: hapster-secret
                  key: KEYCLOAK_CLIENT_SECRET
            - name: KEYCLOAK_REALM_PUBLIC_KEY
              valueFrom:
                secretKeyRef:
                  name: hapster-secret
                  key: KEYCLOAK_REALM_PUBLIC_KEY
            - name: DB_USERNAME
              valueFrom:
                secretKeyRef:
                  name: hapster-secret
                  key: DB_USERNAME
            - name: DB_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: hapster-secret
                  key: DB_PASSWORD
            - name: MAIL_USERNAME
              valueFrom:
                secretKeyRef:
                  name: hapster-secret
                  key: MAIL_USERNAME
            - name: MAIL_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: hapster-secret
                  key: MAIL_PASSWORD
            - name: LRS_USER
              valueFrom:
                secretKeyRef:
                  name: hapster-secret
                  key: LRS_USER
            - name: LRS_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: hapster-secret
                  key: LRS_PASSWORD
          ports:
          - containerPort: 80
            name: http
          volumeMounts:
            - name: storage
              mountPath: /var/www/html/storage
      volumes:
        - name: storage
          persistentVolumeClaim:
            claimName: laravel-pvc