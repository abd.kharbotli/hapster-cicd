#!/bin/bash

# Prompt the user to choose between file or text encoding
read -p "Do you want to encode a file or text? (file/text): " choice

if [[ "$choice" == "file" ]]; then
    # If the user chooses to encode a file
    read -p "Enter the path to the file you want to encode: " file_path

    # Check if the file exists
    if [[ -f "$file_path" ]]; then
        encoded_text=$(echo -n $(cat "$file_path") | base64 -w 0)
        echo -e "\nEncoded text (base64) from $file_path:"
        echo "$encoded_text"
        echo -e "\nEncoded text ends."
    else
        echo "File not found at $file_path."
    fi

elif [[ "$choice" == "text" ]]; then
    # If the user chooses to encode text
    read -p "Enter the text you want to encode: " input_text

    encoded_text=$(echo -n "$input_text" | base64 -w 0)
    echo -e "\nEncoded text (base64):"
    echo "$encoded_text"
    echo -e "\nEncoded text ends."

else
    echo "Invalid choice. Please choose 'file' or 'text'."
fi
