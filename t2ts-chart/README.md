## Introduction

This charts installs translation service and its components into a [Kubernetes](http://kubernetes.io) cluster using the [Helm](https://helm.sh).

## Prerequisites

- Kubernetes 1.28.2
- helm 3.13.0

## Configuration

The following tables lists the configurable parameters of this chart and their default values.

| Parameter                              | Description                               | Default                                             |
| -------------------------------------- | ----------------------------------------- | --------------------------------------------------- |
| `translation.replicas`                 | Number of deployment replicas             | `1`                                                 |
| `translation.hadoopURL`                | The URL of Hadoop cluster                 | `https://hadoop.hapster.com/`                       |
| `translation.hadoopPublicURL`          | The public URL of Hadoop cluster          | `https://hadoop.hapster.com/webhdfs/v1/user/hadoop/`|
| `images.registryCreds`                 | Secret's name of the registry credentials | `registry-creds`                                    |
| `images.translation`                   | Translation container image               | `registry.hapster.dev/translation:1.0.0`            |
| `images.t2t`                           | Text to text container image              | `registry.hapster.dev/t2t:1.0.0`                    |
| `images.t2s`                           | Text to speech container image            | `registry.hapster.dev/t2s:1.0.0`                    |



## Secrets Configuration

The following parameters configure secrets for your application:

| Parameter                                 | Description                               |
| ----------------------------------------- | ----------------------------------------- |
| `secrets.hadoopUser`                      | Hadoop username                           |


## Installing the Chart

To install the chart with the release name `my-release` you have got to do it like this:

```console
$ helm install my-release /path/to/chart/ \
    --set secrets.hadoopUser=<username>\
```

The command deploys the given chart in the default configuration. The [configuration](#configuration) section lists the parameters that can be configured during installation.

> **Tip**: List all releases using `helm list`

## Uninstalling the Chart

To uninstall/delete the `my-release` deployment:

```console
$ helm delete my-release
```

The command removes all the Kubernetes components associated with the chart and deletes the release.